// this is the royalty free game for the purpose of education 


#include "LastStoneWall.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
ULastStoneWall::ULastStoneWall()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void ULastStoneWall::BeginPlay()
{
	Super::BeginPlay();

	start = GetOwner()->GetActorLocation().Z;
	current = start;
	targetz += start;
	// ...
	actoOpenss = GetWorld()->GetFirstPlayerController()->GetPawn();

	FindAuidoCompo();
}


// Called every frame
void ULastStoneWall::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	TArray<AActor*> OverlappingActor;
	
	presPlate->GetOverlappingActors(OUT OverlappingActor);

	for (AActor* acto : OverlappingActor)
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s is on the pressureplate!"), *acto->GetName());
		if (presPlate->IsOverlappingActor(actoOpen))
		{
			//UE_LOG(LogTemp, Warning, TEXT("%s is on the pressureplate!"), *mainActor->GetName());
			OpenLastStone(DeltaTime);
		}

		if (presurePlatess->IsOverlappingActor(actoOpenss))
		{
			//UE_LOG(LogTemp, Warning, TEXT("%s is on the pressureplate!"), *mainActor->GetName());
			UWorld* world = GetWorld();
			if (!ensure(world != nullptr))return;

			world->ServerTravel("/Game/Level/GameClear");
		}


	}

	presurePlatess->GetOverlappingActors(OUT OverlappingActor);

	for (AActor* acto : OverlappingActor)
	{
		if (presurePlatess->IsOverlappingActor(actoOpenss))
		{
			UE_LOG(LogTemp, Warning, TEXT("%s is on the pressureplate!"), *actoOpenss->GetName());
			UWorld* world = GetWorld();
			if (!ensure(world != nullptr))return;

			world->ServerTravel("/Game/Level/GameClear");
		}
	}
}

void ULastStoneWall::OpenLastStone(float DeltaTime)
{
	current = FMath::Lerp(current, targetz, DeltaTime * 0.1f);
	FVector WallLoc = GetOwner()->GetActorLocation();
	WallLoc.Z = current;
	GetOwner()->SetActorLocation(WallLoc);
	if (!audioCompo)return;
	if (!openDoorSound)
	{
		audioCompo->Play();
		openDoorSound = true;
	}
}

void ULastStoneWall::FindAuidoCompo()
{
	audioCompo = GetOwner()->FindComponentByClass<UAudioComponent>();

	if (!audioCompo)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s is missong audio compo"), *GetOwner()->GetName());
	}
}

