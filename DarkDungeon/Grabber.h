// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "GameFramework/Actor.h"
#include "OpenSceretWall.h"
#include "Grabber.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DARKDUNGEON_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	bool flaggss;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	float reach = 500.f;
	
	UPhysicsHandleComponent* physicsHandle = nullptr;
	UInputComponent* inputCompo = nullptr;

	void Grab();
	void Release();
	void FindPhicsHadle();
	void SetUpInputCompo();

	FHitResult getPhysicsBodyInReach() const;
	FVector GetPlayerReach() const;
	FVector GetPlayerWorldPos() const;

	/*UPROPERTY(EditAnywhere)
		USoundBase* impactSound;*/
};
