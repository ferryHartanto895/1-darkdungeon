// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MenuInterface.h"
#include "DarkDungeonGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class DARKDUNGEON_API UDarkDungeonGameInstance : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()

public:
	UDarkDungeonGameInstance(const FObjectInitializer& ObjectInitializer);

	virtual void Init();

	UFUNCTION(BlueprintCallable)
		void LoadMenu();

	UFUNCTION(BlueprintCallable)
		void PauseMenuLoad();

	UFUNCTION(BlueprintCallable)
		void GameOverMenuLoad();

	UFUNCTION(BlueprintCallable)
		void gameClearMenuLoad();

	UFUNCTION(BlueprintCallable)
		void GuideWidgetLoad();

	UFUNCTION(Exec)
		void PlayGames() override;

	UFUNCTION(Exec)
		void Quitss() override;

	virtual void LoadMainMenu() override;
	virtual void LoadGuidePage() override;

private:
	TSubclassOf<class UUserWidget> menuClass;
	TSubclassOf<class UUserWidget> pauseMenu;
	TSubclassOf<class UUserWidget> gameOver;
	TSubclassOf<class UUserWidget> guide;
	TSubclassOf<class UUserWidget> gameClear;

	class UMainMenuWidget* menu;
	class UGameClearWidget* menuys;
	class UGameOverWidget* menuyss;
};
