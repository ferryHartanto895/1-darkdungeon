// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuInterface.h"
#include "MenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class DARKDUNGEON_API UMenuWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	
	void Setup();
	void TearDown();

	void SetMenuInterfacer(IMenuInterface* MainMenuInterface);

protected:
	IMenuInterface* menusInterface;
};
