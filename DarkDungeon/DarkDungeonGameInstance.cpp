// this is the royalty free game for the purpose of education 


#include "DarkDungeonGameInstance.h"
#include "Engine/Engine.h"
#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "MainMenuWidget.h"
#include "GameOverWidget.h"
#include "GameClearWidget.h"
#include "MenuWidget.h"

UDarkDungeonGameInstance::UDarkDungeonGameInstance(const FObjectInitializer& ObjectInitializer)
{
	ConstructorHelpers::FClassFinder<UUserWidget> menuWBPclass(TEXT("/Game/widget/MainMenu"));
	if (!ensure(menuWBPclass.Class != nullptr))return;

	menuClass = menuWBPclass.Class;

	ConstructorHelpers::FClassFinder<UUserWidget> pauseMenuWBPclass(TEXT("/Game/widget/PauseMenu"));
	if (!ensure(pauseMenuWBPclass.Class != nullptr))return;

	pauseMenu = pauseMenuWBPclass.Class;

	ConstructorHelpers::FClassFinder<UUserWidget> gameOverWBPclass(TEXT("/Game/widget/GameOv"));
	if (!ensure(gameOverWBPclass.Class != nullptr)) return;
	gameOver = gameOverWBPclass.Class;

	ConstructorHelpers::FClassFinder<UUserWidget> gameGuideWBPclass(TEXT("/Game/widget/GuidePage"));
	if (!ensure(gameGuideWBPclass.Class != nullptr))return;
	guide = gameGuideWBPclass.Class;
	
	ConstructorHelpers::FClassFinder<UUserWidget> gameClearWBPclass(TEXT("/Game/widget/GameClear"));
	if (!ensure(gameClearWBPclass.Class != nullptr))return;
	gameClear = gameClearWBPclass.Class;

	UE_LOG(LogTemp, Warning, TEXT("GameInstance Constructor"));
}

void UDarkDungeonGameInstance::Init()
{
	//UE_LOG(LogTemp, Warning, TEXT("GameInstance Init"));
	UE_LOG(LogTemp, Warning, TEXT("Found Class %s"), *pauseMenu->GetName());
}

void UDarkDungeonGameInstance::LoadMenu()
{
	if (!ensure(menuClass != nullptr))return;

	menu = CreateWidget<UMainMenuWidget>(this, menuClass);
	if (!ensure(menu != nullptr))return;

	menu->Setup();

	menu->SetMenuInterfacer(this);
}

void UDarkDungeonGameInstance::PauseMenuLoad()
{
	if (!ensure(pauseMenu != nullptr))return;
	UE_LOG(LogTemp, Warning, TEXT("pause menu loaded"));

	UMenuWidget* menus = CreateWidget<UMenuWidget>(this, pauseMenu);
	if (!ensure(menus != nullptr))return;

	menus->Setup();

	menus->SetMenuInterfacer(this);
}

void UDarkDungeonGameInstance::GameOverMenuLoad()
{
	if (!ensure(gameOver != nullptr))return;

	menuyss = CreateWidget<UGameOverWidget>(this, gameOver);
	if (!ensure(menuyss != nullptr))return;

	menuyss->Setup();

	menuyss->SetMenuInterfacer(this);

}

void UDarkDungeonGameInstance::gameClearMenuLoad()
{
	if (!ensure(gameClear != nullptr))return;
	UE_LOG(LogTemp, Warning, TEXT("guide menu loaded"));

	menuys = CreateWidget<UGameClearWidget>(this, gameClear);
	if (!ensure(menuys != nullptr))return;

	menuys->Setup();

	menuys->SetMenuInterfacer(this);
}

void UDarkDungeonGameInstance::GuideWidgetLoad()
{
	if (!ensure(guide != nullptr))return;
	UE_LOG(LogTemp, Warning, TEXT("guide menu loaded"));

	UMenuWidget* menyu = CreateWidget<UMenuWidget>(this, guide);
	if (!ensure(menyu != nullptr))return;

	menyu->Setup();

	menyu->SetMenuInterfacer(this);
}

void UDarkDungeonGameInstance::PlayGames()
{
	if (menu != nullptr)
	{
		menu->TearDown();
	}

	UEngine* engine = GetEngine();
	if (!ensure(engine != nullptr))return;

	engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Go In To The Game"));

	UWorld* world = GetWorld();
	if (!ensure(world != nullptr))return;

	//world->ServerTravel("/Game/Level/CastleOne");
	UGameplayStatics::OpenLevel(GetWorld(), "/Game/Level/CastleOne");
}



void UDarkDungeonGameInstance::Quitss()
{
	if (menu != nullptr)
	{
		menu->TearDown();
	}

	UEngine* engine = GetEngine();
	if (!ensure(engine != nullptr))return;

	engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Exit The Game"));

	UWorld* world = GetWorld();
	if (!ensure(world != nullptr))return;

	UGameViewportClient* const viewPort = world->GetGameInstance()->GetGameViewportClient();
	if (viewPort)
	{
		viewPort->ConsoleCommand("quit");
	}
}

void UDarkDungeonGameInstance::LoadMainMenu()
{
	APlayerController* playerController = GetFirstLocalPlayerController();
	if (!ensure(playerController != nullptr))return;

	playerController->ClientTravel("/Game/Level/mainmenu", ETravelType::TRAVEL_Absolute);

}

void UDarkDungeonGameInstance::LoadGuidePage()
{
	if (!ensure(guide != nullptr))return;
	UE_LOG(LogTemp, Warning, TEXT("guide menu loaded"));

	UMenuWidget* menyu = CreateWidget<UMenuWidget>(this, guide);
	if (!ensure(menyu != nullptr))return;

	menyu->Setup();

	menyu->SetMenuInterfacer(this);
}
