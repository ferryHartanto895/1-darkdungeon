// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/TriggerVolume.h"
#include "LastStoneDoor.generated.h"

UCLASS()
class DARKDUNGEON_API ALastStoneDoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALastStoneDoor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void OpenLastDoor(float DeltaTime);
private:
	float current;
	float start;

	UPROPERTY(EditAnywhere)
	float targetz = -900.f;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* presurePlate;



	UPROPERTY(EditAnywhere)
	AActor* mainact;
};
