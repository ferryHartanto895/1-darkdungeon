// this is the royalty free game for the purpose of education 


#include "Grabber.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Kismet/GameplayStatics.h"

#define OUT
// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	
	
	FindPhicsHadle();
	SetUpInputCompo();

}


// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (!physicsHandle) { return; }
	if (physicsHandle->GrabbedComponent) 
	{
		physicsHandle->SetTargetLocation(GetPlayerReach());
	}
}

void UGrabber::Grab()
{
	FHitResult hitRes = getPhysicsBodyInReach();
	UPrimitiveComponent* CompoToGrab = hitRes.GetComponent();
	AActor* actorHit = hitRes.GetActor();
	if (hitRes.GetActor())
	{
		physicsHandle->GrabComponentAtLocation
		(
			CompoToGrab,
			NAME_None,
			GetPlayerReach()
		);
	}
}

void UGrabber::Release()
{
	if (!physicsHandle) { return; }
	physicsHandle->ReleaseComponent();
}

void UGrabber::SetUpInputCompo()
{
	inputCompo = GetOwner()->FindComponentByClass<UInputComponent>();

	if (inputCompo)
	{
		inputCompo->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		inputCompo->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
}


void UGrabber::FindPhicsHadle()
{
	physicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();

	if (physicsHandle == nullptr) 
	{
		UE_LOG(LogTemp, Warning, TEXT("there is no physicshandle in %s"), *GetOwner()->GetName());
	}
}

FHitResult UGrabber::getPhysicsBodyInReach() const
{
	FHitResult hit;
	
	FCollisionQueryParams traceParams
	(
		FName(TEXT("")),
		false,
		GetOwner()
	);

	GetWorld()->LineTraceSingleByObjectType
	(
		OUT hit,
		GetPlayerWorldPos(),
		GetPlayerReach(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		traceParams
	);
	
	return hit;
}

FVector UGrabber::GetPlayerWorldPos() const
{
	FVector playerviewPointLoc = GetOwner()->GetActorLocation();
	FRotator playerViewPointRot = GetOwner()->GetActorRotation();

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(playerviewPointLoc, playerViewPointRot);
	
	return playerviewPointLoc;
}

FVector UGrabber::GetPlayerReach() const
{
	FVector playerviewPointLoc = GetOwner()->GetActorLocation();
	FRotator playerViewPointRot = GetOwner()->GetActorRotation();

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT playerviewPointLoc,OUT playerViewPointRot);

	return playerviewPointLoc + playerViewPointRot.Vector() * reach;
}

