// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "Components/AudioComponent.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "LastStoneWall.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DARKDUNGEON_API ULastStoneWall : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ULastStoneWall();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void OpenLastStone(float DeltaTime);
	void FindAuidoCompo();

	bool openDoorSound = false;

private:
	float start;
	float current;

	UPROPERTY(EditAnywhere)
	float targetz = -900.f;
	
	UPROPERTY(EditAnywhere)
	ATriggerVolume* presPlate;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* presurePlatess;

	UPROPERTY(EditAnywhere)
	AActor* actoOpen;

	UPROPERTY(EditAnywhere)
		AActor* actoOpenss;

	UPROPERTY()
		UAudioComponent* audioCompo = nullptr;
};
