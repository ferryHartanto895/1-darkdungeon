// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DarkDungeonGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DARKDUNGEON_API ADarkDungeonGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
