// this is the royalty free game for the purpose of education 


#include "LastStoneDoor.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Actor.h"

#define OUT
// Sets default values
ALastStoneDoor::ALastStoneDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALastStoneDoor::BeginPlay()
{
	Super::BeginPlay();
	start = GetOwner()->GetActorLocation().Z;
	current = start;
	targetz += current;
}

// Called every frame
void ALastStoneDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TArray<AActor*> overlapAct;
	presurePlate->GetOverlappingActors(OUT overlapAct);

	for (AActor* act : overlapAct)
	{
		if (presurePlate->IsOverlappingActor(mainact))
		{

		}
	}
}

