// this is the royalty free game for the purpose of education 


#include "GameClearWidget.h"

#include "Kismet/GameplayStatics.h"
#include "Components/Button.h"

bool UGameClearWidget::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	if (!ensure(restartGame != nullptr))return false;
	restartGame->OnClicked.AddDynamic(this, &UGameClearWidget::RestartGame);

	if (!ensure(mainMenuGames != nullptr)) return false;
	mainMenuGames->OnClicked.AddDynamic(this, &UGameClearWidget::MainMenuGamesPressed);

	return true;
}

void UGameClearWidget::RestartGame()
{
	/*UE_LOG(LogTemp, Warning, TEXT("load next level"));*/
	if (menusInterface != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("load next level"));
		TearDown(); 
		UE_LOG(LogTemp, Warning, TEXT("load next level after menu tear down"));
		//menusInterface->PlayGames();
		UGameplayStatics::OpenLevel(GetWorld(), "/Game/Level/CastleOne");
	}
}

void UGameClearWidget::MainMenuGamesPressed()
{
	if (menusInterface != nullptr)
	{
		TearDown();
		menusInterface->LoadMainMenu();
	}
}
