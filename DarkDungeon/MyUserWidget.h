// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MyUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class DARKDUNGEON_API UMyUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
};
