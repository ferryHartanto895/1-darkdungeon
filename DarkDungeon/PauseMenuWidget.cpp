// this is the royalty free game for the purpose of education 

#include "PauseMenuWidget.h"
#include "Components/Button.h"

bool UPauseMenuWidget::Initialize()
{
	bool success = Super::Initialize();
	if (!success)return false;

	if (!ensure(resumeGame != nullptr))return false;
	resumeGame->OnClicked.AddDynamic(this, &UPauseMenuWidget::CancelPRessed);

	if (!ensure(quitGame != nullptr))return false;
	quitGame->OnClicked.AddDynamic(this, &UPauseMenuWidget::QuitPressed);
	
	return true;
}

void UPauseMenuWidget::CancelPRessed()
{
	TearDown();

}

void UPauseMenuWidget::QuitPressed()
{
	if (menusInterface != nullptr)
	{
		TearDown();
		menusInterface->LoadMainMenu();
	}
}
