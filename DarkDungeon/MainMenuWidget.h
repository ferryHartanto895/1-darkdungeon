		// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "MainMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class DARKDUNGEON_API UMainMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
		
public:

protected:
	virtual bool Initialize();

private:
	UPROPERTY(meta = (BindWidget))
		class UButton* playGames;

	UPROPERTY(meta = (BindWidget))
		class UButton* exitGames;

	UPROPERTY(meta = (BindWidget))
		class UButton* Guide;

	UFUNCTION()
		void GuideLoadPage();

	UFUNCTION()
		void PlayGAmes();

	UFUNCTION()
		void ExitGamess();
};
