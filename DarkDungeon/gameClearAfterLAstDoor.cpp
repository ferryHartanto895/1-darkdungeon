// this is the royalty free game for the purpose of education 


#include "gameClearAfterLAstDoor.h"

// Sets default values for this component's properties
UgameClearAfterLAstDoor::UgameClearAfterLAstDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UgameClearAfterLAstDoor::BeginPlay()
{
	Super::BeginPlay();

	if (!presurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("actor has no presureplate compo!"));
	}
	// ...
	
}


// Called every frame
void UgameClearAfterLAstDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	TArray<AActor*> OverlappingActors;
	presurePlate->GetOverlappingActors(OUT OverlappingActors);

	for (AActor* acto : OverlappingActors)
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s is on the pressureplate!"), *acto->GetName());
		if (presurePlate->IsOverlappingActor(mainActor))
		{
			UE_LOG(LogTemp, Warning, TEXT("%s is on the pressureplate!"), *mainActor->GetName());
			UWorld* world = GetWorld();
			if (!ensure(world != nullptr))return;

			world->ServerTravel("/Game/Level/GameClear");

		}
	}
	// ...
}

void UgameClearAfterLAstDoor::gameClearAfterLAstDoor(float DeltaTime)
{

}


