// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "Components/AudioComponent.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "OpenSceretWall.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DARKDUNGEON_API UOpenSceretWall : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenSceretWall();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void OpenSceretWall(float DeltaTime);
	void FindAuidoCompo();

	bool flagg;

	bool openDoorSound = false;
private:
	float start;
	float current;

	UPROPERTY(EditAnyWhere)
	float targetz;

	UPROPERTY(EditAnyWhere)
	ATriggerVolume* presurePlate;
	
	UPROPERTY(EditAnywhere)
	AActor* mainActor;

	UPROPERTY()
		UAudioComponent* audioCompo = nullptr;
};
