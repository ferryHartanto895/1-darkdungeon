// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "GuideWidget.generated.h"

/**
 * 
 */
UCLASS()
class DARKDUNGEON_API UGuideWidget : public UMenuWidget
{
	GENERATED_BODY()
protected:
	virtual bool Initialize();

private:
	UPROPERTY(meta = (BindWidget))
		class UButton* mainButton;

	UFUNCTION()
		void BackToMainMenu();
};
