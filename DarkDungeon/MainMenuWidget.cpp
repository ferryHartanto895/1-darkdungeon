// this is the royalty free game for the purpose of education 


#include "MainMenuWidget.h"
#include "Components/Button.h"


bool UMainMenuWidget::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	if (!ensure(playGames != nullptr))return false;
	playGames->OnClicked.AddDynamic(this, &UMainMenuWidget::PlayGAmes);

	if (!ensure(exitGames!=nullptr)) return false;
	exitGames->OnClicked.AddDynamic(this, &UMainMenuWidget::ExitGamess);

	if (!ensure(Guide != nullptr))return false;
	Guide->OnClicked.AddDynamic(this, &UMainMenuWidget::GuideLoadPage);

	return true;
}



void UMainMenuWidget::GuideLoadPage()
{
	UE_LOG(LogTemp, Warning, TEXT("Go to the game now"));

	if (menusInterface != nullptr)
	{
		menusInterface->LoadGuidePage();
	}
}

void UMainMenuWidget::PlayGAmes()
{
	UE_LOG(LogTemp, Warning, TEXT("Go to the game now"));

	if (menusInterface != nullptr)
	{
		menusInterface->PlayGames();
	}
}

void UMainMenuWidget::ExitGamess()
{
	UWorld* world = GetWorld();
	if (!ensure(world != nullptr))return;

	APlayerController* playerController = world->GetFirstPlayerController();
	if (!ensure(playerController != nullptr))return;

	playerController->ConsoleCommand("quit");
}
