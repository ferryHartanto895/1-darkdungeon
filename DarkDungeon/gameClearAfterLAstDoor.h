// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "gameClearAfterLAstDoor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DARKDUNGEON_API UgameClearAfterLAstDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UgameClearAfterLAstDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void gameClearAfterLAstDoor(float DeltaTime);

private:
	float starter;
	float current;

	UPROPERTY(EditAnywhere)
		ATriggerVolume* presurePlate;

	UPROPERTY(EditAnywhere)
		AActor* mainActor;
		
};
