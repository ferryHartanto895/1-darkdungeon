// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "MainMenuGameMode.generated.h"

/**
 * 
 */
UCLASS()
class DARKDUNGEON_API AMainMenuGameMode : public AGameMode
{
	GENERATED_BODY()
	
};
