// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "MenuInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UMenuInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class DARKDUNGEON_API IMenuInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void PlayGames() = 0;
	virtual void Quitss() = 0;
	virtual void LoadMainMenu() = 0;
	virtual void LoadGuidePage() = 0;
};
