// this is the royalty free game for the purpose of education 

#include "OpenSceretWall.h"
#include "components/PrimitiveComponent.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Actor.h"

#define OUT
// Sets default values for this component's properties
UOpenSceretWall::UOpenSceretWall()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UOpenSceretWall::BeginPlay()
{
	Super::BeginPlay();
	start = GetOwner()->GetActorLocation().Z;
	current = start;
	targetz += start;
	
	mainActor = GetWorld()->GetFirstPlayerController()->GetPawn();

	FindAuidoCompo();
}

void UOpenSceretWall::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	TArray<AActor*> OverlappingActor;
	presurePlate->GetOverlappingActors(OUT OverlappingActor);

	for (AActor* acto : OverlappingActor)
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s is on the pressureplate!"), *acto->GetName());
		if (presurePlate->IsOverlappingActor(mainActor))
		{
				//UE_LOG(LogTemp, Warning, TEXT("%s is on the pressureplate!"), *mainActor->GetName());
				OpenSceretWall(DeltaTime);
		}
	}
}


void UOpenSceretWall::OpenSceretWall(float DeltaTime)
{
	current = FMath::Lerp(current, targetz, DeltaTime * 0.1f);
	FVector WallLoc = GetOwner()->GetActorLocation();
	WallLoc.Z = current;
	GetOwner()->SetActorLocation(WallLoc);

	if (!audioCompo)return;
	if (!openDoorSound)
	{
		audioCompo->Play();
		openDoorSound = true;
	}
}

void UOpenSceretWall::FindAuidoCompo()
{
	audioCompo = GetOwner()->FindComponentByClass<UAudioComponent>();

	if (!audioCompo)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s is missong audio compo"), *GetOwner()->GetName());
	}
}

