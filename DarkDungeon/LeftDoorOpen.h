 

#pragma once

#include "CoreMinimal.h"
#include "Components/AudioComponent.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "LeftDoorOpen.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DARKDUNGEON_API ULeftDoorOpen : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ULeftDoorOpen();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void OpenleftDoor(float DeltaTime);
	void FindAuidoCompo();

	bool openDoorSound = false;

private:
	float start;
	float current;

	UPROPERTY(EditAnywhere)
	float targetYaw = -180.f;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* presurePlate;

	UPROPERTY(EditAnywhere)
	AActor* openningActor;

	UPROPERTY()
		UAudioComponent* audioCompo = nullptr;
};
