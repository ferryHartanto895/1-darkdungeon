// Copyright Epic Games, Inc. All Rights Reserved.

#include "DarkDungeon.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DarkDungeon, "DarkDungeon" );
