// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "GameClearWidget.generated.h"

/**
 * 
 */
UCLASS()
class DARKDUNGEON_API UGameClearWidget : public UMenuWidget
{
	GENERATED_BODY()
protected:
	virtual bool Initialize();

private:
	UPROPERTY(meta = (BindWidget))
		class UButton* restartGame;

	UPROPERTY(meta = (BindWidget))
		class UButton* mainMenuGames;

	UFUNCTION()
		void RestartGame();

	UFUNCTION()
		void MainMenuGamesPressed();
};
