// this is the royalty free game for the purpose of education 


#include "MenuWidget.h"

void UMenuWidget::Setup()
{
	this->AddToViewport();

	UWorld* world = GetWorld();
	if (!ensure(world != nullptr))return;

	APlayerController* playerController = world->GetFirstPlayerController();
	if (!ensure(playerController != nullptr))return;

	FInputModeUIOnly inputModeData;
	inputModeData.SetWidgetToFocus(this->TakeWidget());
	inputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

	playerController->SetInputMode(inputModeData);

	playerController->bShowMouseCursor = true;
}

void UMenuWidget::TearDown()
{
	this->RemoveFromViewport();

	UWorld* world = GetWorld();
	if (!ensure(world != nullptr))return;

	APlayerController* playerContrller = world->GetFirstPlayerController();
	if (!ensure(playerContrller != nullptr))return;

	FInputModeGameOnly InputModeData;
	playerContrller->SetInputMode(InputModeData);

	playerContrller->bShowMouseCursor = false;
}

void UMenuWidget::SetMenuInterfacer(IMenuInterface* MenuInterface)
{
	this->menusInterface = MenuInterface;
}