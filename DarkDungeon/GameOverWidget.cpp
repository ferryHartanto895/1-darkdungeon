// this is the royalty free game for the purpose of education 


#include "GameOverWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Components/Button.h"

bool UGameOverWidget::Initialize()
{
	bool success = Super::Initialize();
	if (!success) return false;

	if (!ensure(restartGame != nullptr))return false;
	restartGame->OnClicked.AddDynamic(this, &UGameOverWidget::RestartGame);

	if (!ensure(mainMenuGames != nullptr)) return false;
	mainMenuGames->OnClicked.AddDynamic(this, &UGameOverWidget::MainMenuGamesPressed);

	return true;
}

void UGameOverWidget::RestartGame()
{
	if (menusInterface != nullptr)
	{
		TearDown();
		UGameplayStatics::OpenLevel(GetWorld(), "/Game/Level/CastleOne");
	}
}

void UGameOverWidget::MainMenuGamesPressed()
{
	if (menusInterface != nullptr)
	{
		TearDown();
		menusInterface->LoadMainMenu();
	}
}
