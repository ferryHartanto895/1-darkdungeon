// this is the royalty free game for the purpose of education 

#pragma once

#include "CoreMinimal.h"
#include "MenuWidget.h"
#include "PauseMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class DARKDUNGEON_API UPauseMenuWidget : public UMenuWidget
{
	GENERATED_BODY()
protected:
	virtual bool Initialize();

private:
	UPROPERTY(meta = (BindWidget))
		class UButton* resumeGame;

	UPROPERTY(meta = (BindWidget))
		class UButton* quitGame;

	UFUNCTION()
		void CancelPRessed();

	UFUNCTION()
		void QuitPressed();
};
