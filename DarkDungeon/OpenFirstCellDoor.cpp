// Fill out your copyright notice in the Description page of Project Settings.


#include "OpenFirstCellDoor.h"
#include "components/PrimitiveComponent.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Actor.h"

#define OUT

// Sets default values for this component's properties
UOpenFirstCellDoor::UOpenFirstCellDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenFirstCellDoor::BeginPlay()
{
	Super::BeginPlay();
	starter = GetOwner()->GetActorLocation().Z;
	current = starter;
	targetZ += starter;

	if (!presurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("actor has no presureplate compo!"));
	}

	FindAuidoCompo();
	//mainActor = GetWorld()->GetFirstPlayerController()->GetPawn();
	// ...
	
}


// Called every frame
void UOpenFirstCellDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	


	TArray<AActor*> OverlappingActors;
	presurePlate->GetOverlappingActors(OUT OverlappingActors);

	for (AActor* acto : OverlappingActors)
	{
		//UE_LOG(LogTemp, Warning, TEXT("%s is on the pressureplate!"), *acto->GetName());
		if (presurePlate->IsOverlappingActor(mainActor))
		{
			//UE_LOG(LogTemp, Warning, TEXT("%s is on the pressureplate!"), *mainActor->GetName());
			OpenFistCellDoor(DeltaTime);

		}
	}

	
}

void UOpenFirstCellDoor::OpenFistCellDoor(float DeltaTime)
{
	current = FMath::Lerp(current, targetZ, DeltaTime * 0.5f);
	FVector doorLoc = GetOwner()->GetActorLocation();
	doorLoc.Z = current;
	GetOwner()->SetActorLocation(doorLoc);

	if (!audioCompo)return;
	if (!openDoorSound)
	{
		audioCompo->Play();
		openDoorSound = true;
	}
}

void UOpenFirstCellDoor::FindAuidoCompo()
{
	audioCompo = GetOwner()->FindComponentByClass<UAudioComponent>();

	if (!audioCompo)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s is missong audio compo"), *GetOwner()->GetName());
	}
}







