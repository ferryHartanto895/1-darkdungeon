
// this is the royalty free game for the purpose of education 


#include "LeftDoorOpen.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/Actor.h"

#define OUT

// Sets default values for this component's properties
ULeftDoorOpen::ULeftDoorOpen()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void ULeftDoorOpen::BeginPlay()
{
	Super::BeginPlay();
	start = GetOwner()->GetActorRotation().Yaw;
	current = start;
	targetYaw += current;
	// ...
	
	FindAuidoCompo();
}


// Called every frame
void ULeftDoorOpen::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	TArray<AActor*> overLappingAct;
	presurePlate->GetOverlappingActors(OUT overLappingAct);

	for (AActor* act : overLappingAct)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s is on plate"), *openningActor->GetName());
		if (presurePlate->IsOverlappingActor(openningActor)) 
		{
			UE_LOG(LogTemp, Warning, TEXT("%s is opennning match"), *openningActor->GetName());
		}
		OpenleftDoor(DeltaTime);
	}

	// ...
}

void ULeftDoorOpen::OpenleftDoor(float DeltaTime)
{
	current = FMath::Lerp(current, targetYaw, DeltaTime * 0.1f);
	FRotator actRot = GetOwner()->GetActorRotation();
	actRot.Yaw = targetYaw;
	GetOwner()->SetActorRotation(actRot);

	if (!audioCompo)return;
	if (!openDoorSound)
	{
		audioCompo->Play();
		openDoorSound = true;
	}
}

void ULeftDoorOpen::FindAuidoCompo()
{
	audioCompo = GetOwner()->FindComponentByClass<UAudioComponent>();

	if (!audioCompo)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s is missong audio compo"), *GetOwner()->GetName());
	}
}

