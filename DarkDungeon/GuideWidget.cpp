// this is the royalty free game for the purpose of education 


#include "GuideWidget.h"
#include "Components/Button.h"

bool UGuideWidget::Initialize()
{
	bool success = Super::Initialize();
	if (!success)return false;
	
	if (!ensure(mainButton != nullptr))return false;
	mainButton->OnClicked.AddDynamic(this, &UGuideWidget::BackToMainMenu);

	return true;
}

void UGuideWidget::BackToMainMenu()
{
	if (menusInterface != nullptr)
	{
		TearDown();
		menusInterface->LoadMainMenu();
	}
}
